# atlCopyPaste

Simple wrapper for copy-paste that uses async await.

See https://github.com/xavi-/node-copy-paste


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-copy-paste.git#master
```

## Example Usage

```javascript
const atlCopyPaste = require('atl-copy-paste')

run()

async function run(){
  await atlCopyPaste.copy('Data123');

  let data = await atlCopyPaste.paste()

  console.log(data)
}
```