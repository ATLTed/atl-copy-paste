var ncp = require("copy-paste");

exports.copy = async function(data){
  return new Promise((resolve, reject)=>{
    ncp.copy(data,()=>{resolve()}); 
  });
}

exports.paste = async function(){
  return new Promise((resolve, reject)=>{
    ncp.paste((err, data)=>{resolve(data)}); 
  });
}